namespace WeatherAPI.Models;

public interface IWeatherService {
  public IEnumerable<string> Cities();
  public VrijemePodatak CityWeather(string city);
}