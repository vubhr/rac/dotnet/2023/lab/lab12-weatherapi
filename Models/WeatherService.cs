namespace WeatherAPI.Models;

public class WeatherService : IWeatherService {
  private readonly Vrijeme vrijeme;

  public WeatherService() {
    vrijeme = new Vrijeme();
  }

  public IEnumerable<string> Cities() {
    return vrijeme.DohvatiGradove();
  }

  public VrijemePodatak CityWeather(string city) {
    return vrijeme.DohvatiPodatkeZaGrad(city);
  }
}