using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WeatherAPI.Models;

namespace WeatherAPI.Controllers;

[Route("api/v1/weather")]
[ApiController]
public class WeatherController : ControllerBase
{
    private readonly IWeatherService ws;

    public WeatherController(IWeatherService ws) {
      this.ws = ws;
    }

    [HttpGet]
    public IEnumerable<string> Weather()
    {
        return ws.Cities();
    }

    [HttpGet("{city}")]
    public ActionResult<VrijemePodatak> WeatherForCity(string? city) {
      if (city == null) {
        return NotFound();
      }

      return ws.CityWeather(city);
    }
}
